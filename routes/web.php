<?php

use App\Http\Controllers\Admin\ActiviteController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AnnonceController;
use App\Http\Controllers\Admin\CalendrierController;
use App\Http\Controllers\Admin\CatActiviteController;
use App\Http\Controllers\Admin\DocumentController;
use App\Http\Controllers\Front\ActivitesFController;
use App\Http\Controllers\Front\AnnonceController as FrontAnnonceController;
use App\Http\Controllers\Front\FCalendrierController;
use App\Http\Controllers\Front\FDocumentsController;
use App\Http\Controllers\Front\FTrombinoscopeController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





// Route::get('/', function () {
//     return view('welcome');
// });


// Front Groupe Route

// Route::get('/',[HomeController::class,'index'])->name('home');
Route::get('/activites',[ActivitesFController::class,'index'])->name('activites_front');       
Route::get('/activites.show/{id}',[ActivitesFController::class,'show'])->name('activites_show');    

        // annonces

Route::get('/annonces',[FrontAnnonceController::class,'index'])->name('annonce_front');

// Document

Route::get('/documents',[FDocumentsController::class,'index'])->name('documents_front');

// Trombinoscope

Route::get('/trombinoscope',[FTrombinoscopeController::class,'index'])->name('trombinoscope_front');

// Calendrier

Route::get('/calendrier',[FCalendrierController::class,'index'])->name('calendrier_front');
// end front groupe route





// admin Route groute

Route::get('/index.Dashbord',[AdminController::class,'index'])->name('index.Dashbord');

Route::get('/index.document',[DocumentController::class,'index'])->name('index.Document');
Route::get('/create.document',[DocumentController::class,'create'])->name('create.Document');

Route::get('/index.annonce',[AnnonceController::class,'index'])->name('index.Annonce');
Route::get('/create.annonce',[AnnonceController::class,'create'])->name('create.Annonce');
route::post('/store.annonce',[AnnonceController::class,'store'])->name('store.annonce');
route::get('/edit.annonce/{id}',[AnnonceController::class,'edit'])->name('edit.annonce');
route::post('/update.annonce/{id}',[AnnonceController::class,'update'])->name('update.annonce');
route::get('/destroy.annonce/{id}',[AnnonceController::class,'destroy'])->name('destroy.annonce');



route::get('/index.activite',[ActiviteController::class,'index'])->name('index.activite');
route::get('/create.activite',[ActiviteController::class,'create'])->name('create.activite');
route::post('/store.activite',[ActiviteController::class,'store'])->name('store.activite');
// route::get('/show.activite',[ActiviteController::class,'show'])->name('show.activite');
route::get('/edit.activite/{id}',[ActiviteController::class,'edit'])->name('edit.activite');
route::post('/update.activite/{id}',[ActiviteController::class,'update'])->name('update.activite');
route::get('/destroy.activite/{id}',[ActiviteController::class,'destroy'])->name('destroy.activite');

// route activité categorie
route::get('/index.Cat-activite',[CatActiviteController::class,'index'])->name('index.cat-activite');
route::get('/create.Cat-activite',[CatActiviteController::class,'create'])->name('create.cat-activite');
route::post('/store.Cat-activite',[CatActiviteController::class,'store'])->name('store.cat-activite');
// route::get('/edit.activite/{id}',[ActiviteController::class,'edit'])->name('edit.activite');
// route::post('/update.activite/{id}',[ActiviteController::class,'update'])->name('update.activite');
// route::get('/destroy.activite/{id}',[ActiviteController::class,'destroy'])->name('destroy.activite');


route::get('/index.calendrier',[CalendrierController::class,'index'])->name('index.calendrier');
route::get('/create.calendrier',[CalendrierController::class,'create'])->name('create.calendrier');
route::post('/store.calendrier',[CalendrierController::class,'store'])->name('store.calendrier');
route::get('/edit.calendrier/{id}',[CalendrierController::class,'edit'])->name('edit.calendrier');
route::post('/update.calendrier/{id}',[CalendrierController::class,'update'])->name('update.calendrier');
route::get('/destroy.calendrier/{id}',[CalendrierController::class,'destroy'])->name('destroy.calendrier');

// Route::get('/', [HomeController::class, 'index'])->name('home');

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::prefix('user')->name('user.')->group(function(){
  
        Route::middleware(['guest:web','PreventBackHistory'])->group(function(){
              Route::view('/','Front.pages.login')->name('login');
              Route::view('/register','Front.pages.register')->name('register');
              Route::post('/create',[UserController::class,'create'])->name('create');
              Route::post('/check',[UserController::class,'check'])->name('check');
        });
    
        Route::middleware(['auth:web','PreventBackHistory'])->group(function(){
        //       Route::view('/home','Front.pages.home')->name('home');
              Route::get('/home_mcen',[HomeController::class,'index'])->name('home');
              Route::post('/logout',[UserController::class,'logout'])->name('logout');
              Route::get('/add-new',[UserController::class,'add'])->name('add');
        });
    
//     });
    