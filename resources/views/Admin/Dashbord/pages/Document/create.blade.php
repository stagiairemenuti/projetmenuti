@extends('Admin.Dashbord.layout.main')

@section('section')

<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
           
            <div class="col-lg-8 px-auto mx-auto">
                <div class="card">
                    <div class="card-header">Document</div>
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center title-2">Ajouter un Document</h3>
                        </div>
                        <hr>
                        <form action="" method="post" novalidate="novalidate">
                            <div class="form-group mb-3">
                                <label for="cc-payment" class="control-label mb-1">Categories</label>
                                <div class="col-12">
                                    <select name="select" id="select" class="form-control">
                                       <option value="0">Circulaires</option>
                                         <option value="1">Avenant</option>
                                        <option value="2">Convention</option>
                                         <option value="3">Option #3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <div class="col-12">
                                    <label for="file-input" class=" form-control-label">Charger Document</label>
                                </div>
                                <div class="col-12">
                                    <input type="file" id="file-input" name="file-input" class="form-control-file">
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label for="cc-name" class="control-label mb-1">Titre</label>
                                <input id="cc-name" name="cc-name" type="text" class="form-control cc-name valid" data-val="true" data-val-required="Please enter the name on card"
                                    autocomplete="cc-name" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">
                                <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="cc-exp" class="control-label mb-1">Date</label>
                                        <input id="cc-exp" name="cc-exp" type="tel" class="form-control cc-exp" value="" data-val="true" data-val-required="Please enter the card expiration"
                                            data-val-cc-exp="Please enter a valid month and year" placeholder="MM / YY"
                                            autocomplete="cc-exp">
                                        <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                    <span id="payment-button-amount">Publier le Document</span>
                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
           
            <div class="col-md-4 shadow-lg p-3 mb-5 bg-white rounded">
                                    <div class="card-header">
                                        ADD New<strong>Category</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="" method="post" class="form-inline">
                                            <div class="form-group col-md-6 m-0">
                                                <label for="exampleInputName2" class="pr-1  form-control-label">Name</label>
                                                <input type="text" id="exampleInputName2" placeholder="Jane Doe" required="" class="form-control">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Ajouter
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm ms-3">
                                            <i class="fa fa-dot-circle-o"></i> Liste
                                        </button>
                                    </div>
                                </div>
        </div>
        
    </div>
</div>
    
@endsection