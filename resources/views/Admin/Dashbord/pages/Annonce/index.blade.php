@extends('Admin.Dashbord.layout.main');

@section('section')
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-sm-3 col-xs-12">
                                <h4 class="title">Annonce <span>List</span></h4>
                            </div>
                            <div class="col-sm-9 col-xs-12 text-right">
                                <div class="btn_group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <button class="btn btn-default" title="Reload"><i class="fa fa-sync-alt"></i></button>
                                    <a href="{{route('create.Annonce')}}"><button class="btn btn-default" title="ADD New"><i class="fa fa-plus" aria-hidden="true"></i></button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Année</th>
                                    <th>Libelle</th>
                                    <th>titre</th>
                                    <th>Publier Le</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @forelse ($annonces as $annonce)
                                    <tr>
                                        <td>{{$annonce->date}}</td>
                                        <td>{{$annonce->libelle}}</td>
                                        <td>{{$annonce->titre}}</td>
                                        <td>{{$annonce->created_at}}</td>
                                        
                                        <td>
                                            <ul class="action-list">
                                                <li><a href="{{route('edit.annonce',$annonce->id)}}" data-tip="edit"><i class="fa fa-edit"></i></a></li>
                                                <li><a href="{{route('destroy.annonce',$annonce->id)}}" data-tip="delete"><i class="fa fa-trash"></i></a></li>
                                            </ul>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="5">Pas d'annonce</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>


@endsection