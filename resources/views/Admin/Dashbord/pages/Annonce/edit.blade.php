@extends('Admin.Dashbord.layout.main')


@section('section')
    


<div class="row">
    <div class="col-lg-8 col-md-8 px-auto mx-auto">
        <div class="card">
            <div class="card-header">
                <strong>Modifier une Annonce</strong>
            </div>
            <div class="card-body card-block">
                <form action="{{route('update.annonce',$editannonce->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                      
                    @if (Session::get('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    @if (Session::get('fail'))
                        <div class="alert alert-danger">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                    @csrf

                    {{-- <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="type_id" class=" form-control-label">Type</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="type_id" id="type_id" class="form-control" value="{{$editannonce->type_id}}">
                                <option value="0">Type select</option>
                                <option value="1">Option #1</option>
                                <option value="2">Option #2</option>
                                <option value="3">Option #3</option>
                            </select>
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </div>
                    </div>  --}}

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="titre" class=" form-control-label">Titre</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="titre" id="titre" name="titre"  class="form-control" value="{{$editannonce->titre}}">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="date" class=" form-control-label">Année</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="date" id="date" name="date"  class="form-control" value="{{$editannonce->date}}">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="libelle" class=" form-control-label">Libelle</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <textarea name="libelle" id="libelle" rows="9"  value="{{$editannonce->libelle}}" class="form-control">{{$editannonce->libelle}}</textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-md">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>


@endsection