@extends('Admin.Dashbord.layout.main')


@section('section')
    


<div class="row">
    <div class="col-lg-8 col-md-8 px-auto mx-auto">
        <div class="card">
            <div class="card-header">
                <strong>Ajouter une nouvelle Annonce</strong>
            </div>
            <div class="card-body card-block">
                <form action="{{route('store.annonce')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                      
                    @if (Session::get('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    @if (Session::get('fail'))
                        <div class="alert alert-danger">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                    @csrf

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="titre" class=" form-control-label">Titre</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="titre" id="titre" name="titre" placeholder="titre annonce" class="form-control">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="date" class=" form-control-label">Année</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="date" id="date" name="date" placeholder="MM/YY" class="form-control">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="libelle" class=" form-control-label">Libelle</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <textarea name="libelle" id="libelle" rows="9"  placeholder="Text..." class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-md">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>


@endsection