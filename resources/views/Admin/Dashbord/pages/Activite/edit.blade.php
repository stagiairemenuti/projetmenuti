@extends('Admin/Dashbord/layout/main')

@section('section')
    <div class="row">
        <div class="container mt-5 mb-5 col-lg-8 col-md-7">
            <div class="card px-1 py-4">
                <div class="card-body">
                    <h2 class="card-title mb-3">Modifier l'Activitée</h2>
                    
                   <form action="{{ route('update.activite',$editactivite->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        @if (Session::get('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    @if (Session::get('fail'))
                        <div class="alert alert-danger">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                    @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="category_Id">Categorie</label>
                                <div class="col-12">
                                    <select name="category_Id" id="category_Id" class="form-control">
                                        <option value="Ministre">Ministre</option>
                                          <option value="Secretariat General">Secretariat General</option>
                                         <option value="Cabinet">Cabinet</option>
                                     </select>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="titre">Titre</label>
                                <input class="form-control" type="text" name="titre" placeholder="{{$editactivite->titre}}" value="{{$editactivite->titre}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="libelle">Contenue</label>
                                {{-- <textarea class="form-control" type="text" name="titre" placeholder="Titre ..."> --}}
                                    <textarea class="form-control" name="libelle" id="libelle" cols="30" rows="10" value='{{$editactivite->libelle}}'>{{$editactivite->libelle}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="image">Image</label>
                                <div class="col-12">
                                    <input class="form-control" type="file" name="image" value="{{$editactivite->image}}">
                                </div>
                            </div>
                        </div>
                    </div>
                     <button class="btn btn-primary btn-block confirm-button">Confirmer Modification</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection
