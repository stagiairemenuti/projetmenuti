@extends('Admin/Dashbord/layout/main')

@section('section')
<div class="container">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <div class="panel">
                @if (Session::get('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (Session::get('fail'))
                <div class="alert alert-danger">
                    {{ Session::get('fail') }}
                </div>
            @endif
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <a href="{{route('create.activite')}}" class="btn btn-sm btn-primary pull-left"><i class="fa fa-plus-circle"></i> Add New</a>
                           
                        </div>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Categorie</th>
                                <th>Titre</th>
                                <th>Date</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>

                            @forelse ($activites as $activite)
                                <tr>
                                    <td>
                                        <ul class="action-list">
                                            <li><a href="{{route('edit.activite',$activite->id)}}" class="btn btn-primary"><i class="fa fa-pencil-alt"></i></a></li>
                                            <li><a href="{{route('destroy.activite',$activite->id)}}" class="btn btn-danger"><i class="fa fa-times"></i></a></li>
                                        </ul>
                                    </td>
                                    <td>{{$activite->category_Id}}</td>
                                    <td>{{$activite->titre}}</td>
                                    <td>24/10/2022</td>
                                    <td>
                                        <div class="doc-img">
                                            <img src="{{ asset("uploads/$activite->image") }}" alt="">
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <td><span class="text-danger text-center" colspan="5">Aucun article ajouter</span></td>
                            @endforelse
                         
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection