@extends('Admin/Dashbord/layout/main')

@section('section')
    <div class="row">
        <div class="col-md-6">
            <div class="card-header">
               Ajouter <strong>Categorie</strong>
            </div>
            <div class="card-body card-block">
                <form action="{{route('store.cat-activite')}}" method="post" class="form-horizontal">
                    @if (Session::get('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                        @endif
                        @if (Session::get('fail'))
                            <div class="alert alert-danger">
                                {{ Session::get('fail') }}
                            </div>
                    @endif
                    @csrf
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="titre" class=" form-control-label">Titre</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text"  name="titre" placeholder="Enter Activite Cat..." class="form-control">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Creer New
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Liste
                        </button>
                    </div>
                </form>
            </div>
            
        </div>
        <div class="col-md-offset-1 col-md-4">
            <div class="panel">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-sm-3 col-xs-12">
                            <h4 class="title">Categorie <span>Liste</span></h4>
                        </div>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>titre</th>
                                <th>Ajouter Le</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                           
                            @forelse ( $catactivites as $catactivite)
                                <tr>
                                    <td>{{$catactivite->titre}}</td>
                                    <td>{{$catactivite->created_at}}</td>
                                    
                                    <td>
                                        <ul class="action-list">
                                            <li><a href="" data-tip="edit"><i class="fa fa-edit"></i></a></li>
                                            <li><a href="" data-tip="delete"><i class="fa fa-trash"></i></a></li>
                                        </ul>
                                    </td>
                                </tr>
                        
                                
                            @empty
                                <tr>
                                    <td class="text-center" colspan="5">Pas d'annonce</td>
                                </tr>
                            @endforelse
                         
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
    
@endsection
