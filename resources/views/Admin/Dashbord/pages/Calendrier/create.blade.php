@extends('Admin.Dashbord.layout.main')

@section('section')

<div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
           
            <div class="col-lg-8 px-auto mx-auto">
                <div class="card">
                    <div class="card-header text-primary">Calendrier</div>
                    <div class="card-body">
                        <div class="card-title">
                            <h3 class="text-center title-2 text-primary">Ajouter un calendrier</h3>
                        </div>
                        <hr>
                        <form action="{{route('store.calendrier')}}" method="POST" enctype="multipart/form-data">
                            @if (Session::get('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                            @endif
                            @if (Session::get('fail'))
                                <div class="alert alert-danger">
                                    {{ Session::get('fail') }}
                                </div>
                            @endif
                            @csrf
                            
                            
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="titre" class=" form-control-label">Titre</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="titre" name="titre" placeholder="titre" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="date" class=" form-control-label">Date</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="date" id="date" name="date" placeholder="titre" class="form-control">
                                </div>
                            </div>
                            <div>
                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                                    <span id="payment-button-amount">Publier le Calendrier</span>
                                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
           
           
        </div>
        
    </div>
</div>
    
@endsection