@extends('Front/layout/main')


@section('title')
    <title>Calendrier</title>
@endsection


@section('content')

<section id="faq" class="faq pt-7 pb-0" style="border-left: solid 1px #ed502e; border-right: solid 1px #ed502e; margin: 50px">
    <div class="container m-2 p-0 " data-aos="fade-up">
      <div class="section-title">
        <h2>Calendrier</h2>
        <p>Calendrier</p>
      </div>

      <div class="wrapper pb-0 pt-0">
        <main>
          <div class="toolbar">
            <div class="current-month">Evènement de la semaine</div>
          </div>
          <div class="calendar">
            <div class="calendar__header">
              <div>mon</div>
              <div>tue</div>
              <div>wed</div>
              <div>thu</div>
              <div>fri</div>
              <div>sat</div>
              <div>sun</div>
            </div>
            <div class="calendar__week">
              <div class="calendar__day day">1</div>
              <div class="calendar__day day">2</div>
              <div class="calendar__day day">3</div>
              <div class="calendar__day day">4</div>
              <div class="calendar__day day">5</div>
              <div class="calendar__day day">6</div>
              <div class="calendar__day day">7</div>
            </div>
            <div class="calendar__week">
              <div class="calendar__day day">8</div>
              <div class="calendar__day day">9</div>
              <div class="calendar__day day">10</div>
              <div class="calendar__day day">11</div>
              <div class="calendar__day day">12</div>
              <div class="calendar__day day">13</div>
              <div class="calendar__day day">14</div>
            </div>
            <div class="calendar__week">
              <div class="calendar__day day">15</div>
              <div class="calendar__day day">16</div>
              <div class="calendar__day day">17</div>
              <div class="calendar__day day">18</div>
              <div class="calendar__day day">19</div>
              <div class="calendar__day day">20</div>
              <div class="calendar__day day">21</div>
            </div>
            <div class="calendar__week">
              <div class="calendar__day day">22</div>
              <div class="calendar__day day">23</div>
              <div class="calendar__day day">24</div>
              <div class="calendar__day day">25</div>
              <div class="calendar__day day">26</div>
              <div class="calendar__day day">27</div>
              <div class="calendar__day day">28</div>
            </div>
            <div class="calendar__week">
              <div class="calendar__day day">29</div>
              <div class="calendar__day day">30</div>
              <div class="calendar__day day">31</div>
              <div class="calendar__day day">1</div>
              <div class="calendar__day day">2</div>
              <div class="calendar__day day">3</div>
              <div class="calendar__day day">4</div>
            </div>
          </div>
        </main>
      </div>


    </div>
</section>
@endsection
