@extends('Front/layout/main')

@section('title')
    <title>Trombinoscope</title>
@endsection


@section('content')
<section id="faq" class="faq pt-7 px-0 mx-0">
    <div class="container">
      <div class="section-title">
        <h2>Trombinoscope</h2>
        <p>Trombinoscope</p>
      </div>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="card btn btn-body" style="width: 500px; heigth: 500px;" data-bs-toggle="modal" data-bs-target="#info">
                <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/ministre.jpeg') }}"  alt="Card image cap">
                <div class="card-body justify-content-center text-center">
                    <h5 class="card-title text-center">Le Ministre</h5>
                    <p class="card-text"><strong>Adom Roger Felix</strong></p>
                </div>
                {{-- Debut du modal ministre --}}
                <div class="modal" id="info">
                    <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                              <h5>Le Ministre</h5>
                          </div>
                          <div class="modal-body">
                              {{-- Image de la personne --}}
                              <div class="row">
                                  <div class="col-6">
                                    <img src="{{ ('Front/assets/img/trombinoscope/ministre.jpeg') }}" style="width: 100%" alt="">
                                  </div>
                                  <div class="col-6">
                                        Adom Roger Felix
                                  </div>
                              </div>

                          </div>
                        </div>
                    </div>
                </div>
                {{-- Fin du modal ministre --}}

            </div>
        </div>

        <div class="row mt-3">

            <div class="col-2">
                <div class="card btn btn-body" style="width: 200px;" data-bs-toggle="modal" data-bs-target="#personne1">
                    <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                    <div class="card-body">
                    <h5 class="card-title  text-center">John Doh</h5>

                    </div>
                </div>
                <div class="modal" id="personne1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Titre de la personne</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-6">
                                        <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                                    </div>
                                    <div class="col-6">
                                         John Doh
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-2">
                <div class="card btn btn-body" style="width: 200px;" data-bs-toggle="modal" data-bs-target="#personne1">
                    <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                    <div class="card-body">
                    <h5 class="card-title  text-center">John Doh</h5>

                    </div>
                </div>
                <div class="modal" id="personne1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Titre de la personne</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-6">
                                        <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                                    </div>
                                    <div class="col-6">
                                         John Doh
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-2">
                <div class="card btn btn-body" style="width: 200px;" data-bs-toggle="modal" data-bs-target="#personne1">
                    <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                    <div class="card-body">
                    <h5 class="card-title  text-center">John Doh</h5>

                    </div>
                </div>
                <div class="modal" id="personne1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Titre de la personne</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-6">
                                        <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                                    </div>
                                    <div class="col-6">
                                         John Doh
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-2">
                <div class="card btn btn-body" style="width: 200px;" data-bs-toggle="modal" data-bs-target="#personne1">
                    <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                    <div class="card-body">
                    <h5 class="card-title  text-center">John Doh</h5>

                    </div>
                </div>
                <div class="modal" id="personne1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Titre de la personne</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-6">
                                        <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                                    </div>
                                    <div class="col-6">
                                         John Doh
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="card btn btn-body" style="width: 200px;" data-bs-toggle="modal" data-bs-target="#personne1">
                    <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                    <div class="card-body">
                    <h5 class="card-title  text-center">John Doh</h5>

                    </div>
                </div>
                <div class="modal" id="personne1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Titre de la personne</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-6">
                                        <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                                    </div>
                                    <div class="col-6">
                                         John Doh
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="card btn btn-body" style="width: 200px;" data-bs-toggle="modal" data-bs-target="#personne1">
                    <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                    <div class="card-body">
                    <h5 class="card-title  text-center">John Doh</h5>

                    </div>
                </div>
                <div class="modal" id="personne1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Titre de la personne</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-6">
                                        <img class="card-img-top" src="{{ ('Front/assets/img/trombinoscope/photo1.jpg') }}" alt="Card image cap">
                                    </div>
                                    <div class="col-6">
                                         John Doh
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>
    </div>

    </div>
</section>
@endsection
