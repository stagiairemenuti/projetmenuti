
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.bootstrapdash.com/demo/miri-ui-kit-free/demo/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Apr 2022 03:40:49 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{asset("assets/images/favicon.ico")}}" type="image/x-icon">
    <title>MCEN - Register</title>

    <!-- Vendor css -->
    <link rel="stylesheet" href="{{asset("src/vendors/%40mdi/font/css/materialdesignicons.min.css")}}">

    <!-- Base css with customised bootstrap included -->
    <link rel="stylesheet" href="{{asset("src/css/miri-ui-kit-free.css")}}">

    <link href="{{asset('Admin/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('Admin/vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('Admin/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
    <script src="https://kit.fontawesome.com/2b6a213e82.js" crossorigin="anonymous"></script>

    <!-- Stylesheet for demo page specific css -->
    <link rel="stylesheet" href="{{asset("assets/css/demo.css")}}">
</head>
<body class="login-page">
    <header class="miri-ui-kit-header header-no-bg-img header-navbar-only">
        <nav class="navbar navbar-expand-lg navbar-dark bg-transparent">
            <div class="container">
                <a class="navbar-brand" href="index.html"><img src="{{asset("Admin/images/amoirie.png")}}" alt="logo" ></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#miriUiKitNavbar"
                    aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="mdi mdi-menu"></span>
                </button>
            
                <div class="collapse navbar-collapse" id="miriUiKitNavbar">
                    <div class="navbar-nav ml-auto">
                       
                        <li class="nav-item">

                            <a class="nav-link nav-icon icon-fb" href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i>
                            </a>

                            <a class="nav-link nav-icon icon-twitter mx-3" href="#"><i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
 
                            <a class="nav-link nav-icon icon-insta" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                            
                        </li>
            
                        <a href="#" class="ml-5"><button type="submit" class="btn btn-danger">Contactez Nous</button></a>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <div class="card login-card">
        <div class="card-body">
            <h3 class="text-center text-white font-weight-light mb-4">Inscription</h3>
            <form action="{{ route('create') }}" method="post" autocomplete="off">
                @if (Session::get('success'))
                     <div class="alert alert-success">
                         {{ Session::get('success') }}
                     </div>
                @endif
                @if (Session::get('fail'))
                <div class="alert alert-danger">
                    {{ Session::get('fail') }}
                </div>
                @endif

                @csrf
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" placeholder="Saisir une adresse" value="{{ old('email') }}">
                    <span class="text-danger">@error('email'){{ $message }} @enderror</span>
                </div>
                  <div class="form-group">
                      <label for="password">Mot De Passe</label>
                      <input type="password" class="form-control" name="password" placeholder="Enter Mot de passe" value="{{ old('password') }}">
                      <span class="text-danger">@error('password'){{ $message }} @enderror</span>
                  </div>
                  <div class="form-group">
                    <label for="cpassword">Confirmer Mot de Passe</label>
                    <input type="password" class="form-control" name="cpassword" placeholder="confirmer Mot de passe" value="{{ old('cpassword') }}">
                    <span class="text-danger">@error('cpassword'){{ $message }} @enderror</span>
                </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-primary">s'inscrire</button>
                  </div>
                  <br>
                  <a href="{{ route('login') }}">J'ai déjà un compte</a>
              </form>
            {{-- <div class="d-flex justify-content-center mt-4">
                <p class="text-center mb-0">
                    <a href="{{ route('register') }}" class="social-login-btn icon-fb fs-6">Créer un compte
                    </a>
                   
                </p>
            </div> --}}
        </div>
    </div>
    <footer>
        <div class="container">
            <nav class="navbar navbar-dark bg-transparent navbar-expand d-block d-sm-flex text-center justify-content-between">
                <div class="d-flex">
                    <span class="navbar-text py-0">Copyright © </span><a href="#" target="_blank" class="text-white pl-1"> Telecom.gouv.ci</a><span class="navbar-text py-0 pl-1">2020</span>
                </div>
                
            </nav>
        </div>
    </footer>
    <script src="{{asset("src/vendors/jquery/dist/jquery.min.js")}}"></script>
    <script src="{{("src/vendors/popper.js/dist/umd/popper.min.js")}}"></script>
    <script src="{{asset("src/vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <script src="https://kit.fontawesome.com/2b6a213e82.js" crossorigin="anonymous"></script>
</body>

<!-- Mirrored from www.bootstrapdash.com/demo/miri-ui-kit-free/demo/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Apr 2022 03:40:52 GMT -->
</html>
