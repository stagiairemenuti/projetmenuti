@extends('Front/layout/main')


@section('title')
    <title>Documents </title>
@endsection


@section('content')
<section id="faq" class="faq pt-7" style="border-left: solid 1px #ed502e; border-right: solid 1px #ed502e; margin: 50px">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Documents</h2>
        <p>Documents</p>
      </div>

      <div class="row">
        <div class="season_tabs">

            <div class="season_tab">
                <input type="radio" id="tab-1" name="tab-group-1" checked>
                <label for="tab-1">Journal officiel</label>

                <div class="season_content">
                    <span>Document journal officiel</span>

                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-2" name="tab-group-1">
                <label for="tab-2">Arrêtés</label>

                <div class="season_content">
                    <span>Liste de documents Arretes</<span>
                </div>
            </div>

             <div class="season_tab">
                <input type="radio" id="tab-3" name="tab-group-1">
                <label for="tab-3">Attestation SGG</label>

                <div class="season_content">
                    <span>Liste de doccument Attestation SGG</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-4" name="tab-group-1">
                <label for="tab-4">Avenants</label>

                <div class="season_content">
                    <span>Liste de document avenants</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-5" name="tab-group-1">
                <label for="tab-5">Circulaires</label>

                <div class="season_content">
                    <span>Liste de document circulaire</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-6" name="tab-group-1">
                <label for="tab-6">Contrats</label>

                <div class="season_content">
                    <span>Liste de document de contrats</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-7" name="tab-group-1">
                <label for="tab-7">Convention</label>

                <div class="season_content">
                    <span>Liste de document de convention</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-8" name="tab-group-1">
                <label for="tab-8">CPEAO</label>

                <div class="season_content">
                    <span>Liste de document CPEAO</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-9" name="tab-group-1">
                <label for="tab-9">Décisions</label>

                <div class="season_content">
                    <span>Liste de document de Décisions</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-10" name="tab-group-1">
                <label for="tab-10">Décrêts</label>

                <div class="season_content">
                    <span>Liste de document de Décrêts</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-11" name="tab-group-1">
                <label for="tab-11">Lois</label>

                <div class="season_content">
                    <span>Liste de document de Lois</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-12" name="tab-group-1">
                <label for="tab-12">Mémorandum</label>

                <div class="season_content">
                    <span>Liste de document de Mémorandum</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-13" name="tab-group-1">
                <label for="tab-13">Ordonnances</label>

                <div class="season_content">
                    <span>Liste de document de Ordonnances</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-14" name="tab-group-1">
                <label for="tab-14">Protocoles d'accord</label>

                <div class="season_content">
                    <span>Liste de document de Protocoles d'accord</span>
                </div>
            </div>

            <div class="season_tab">
                <input type="radio" id="tab-15" name="tab-group-1">
                <label for="tab-15">Statut</label>

                <div class="season_content">
                    <span>Liste de document de Statut</span>
                </div>
            </div>

         </div>

    </div>
</section>
@endsection
