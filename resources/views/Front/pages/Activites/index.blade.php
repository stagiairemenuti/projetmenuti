@extends('Front/layout/main')

@section('title')
    <title>Activités</title>
@endsection

@section('content')
    {{-- Actualites --}}
    <section id="portfolio" class="portfolio" style="border-left: solid 1px #ed502e; border-right: solid 1px #ed502e; margin: 20px">
        <div class="container" data-aos="fade-up">

            <div class="section-title pt-3">
                <h2>Actualités</h2>
                <p>Actualités</p>
            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-12 d-flex justify-content-center">
                <ul id="portfolio-flters">
                    
                    @foreach ($Catactivites as $Catactivite)
                        <li data-filter=".{{$Catactivite->id}}" class="filter">{{$Catactivite->titre}}</li>
                    @endforeach
                </ul>
                </div>
            </div>

            <div class="row portfolio-container p-0 m-0 col-md-12" data-aos="fade-up" data-aos-delay="200">


                
                @forelse ($activites as $activite)
                    <div class="col-lg-4 col-md-4 portfolio-item filter-card {{$activite->CatByActivite->id}}">
                      <a href="{{ route('activites_show', $activite->id) }}">
                        <img src="{{ asset("uploads/$activite->image") }}" class="img-fluid" alt="" style="min-width:250px !important">
                        <div class="portfolio-info">
                            <h4>{{ $activite->titre }}</h4>
                            <a href="{{ asset("uploads/$activite->image") }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 3">  </a>
                            <a href="#" class="details-link" title="More Details">  </a>
                        </div>
                      </a>
                    </div>
                @empty
                    <span>Aucune Activité ajoutée</span>
                @endforelse

            </div>
        </div>
    </section>
    {{-- End Antualites --}}
@endsection