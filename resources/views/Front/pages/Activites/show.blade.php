
@extends('Front/layout/main')

@section('title')
    <title>Activités - <!--- le titre  de l---></title>
@endsection

@section('content')
    {{-- Actualites --}}
    <section id="portfolio" class="portfolio" style="border-left: solid 1px #ed502e; border-right: solid 1px #ed502e; margin: 20px">
        <div class="container" data-aos="fade-up">
            {{-- < class="section-title pt-3">--}}
                <h2>Actualités</h2>
                <p>Actualités</p>
        </div>

        <div class="article">
            <div class="row">
                <div class="col-8">
                    <div class="row">
                        <div class="col">
                            {{$detailactivite->titre}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <img src="{{ asset("uploads/$detailactivite->image") }}" alt="" style="min-width: 500px"> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            {{$detailactivite->libelle}}
                        </div>
                    </div>

                </div>

                <div class="col-4">

                </div>
            </div>
        </div>
    </section>
    {{-- End Antualites --}}
@endsection
