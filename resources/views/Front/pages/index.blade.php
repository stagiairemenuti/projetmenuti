@extends('Front/layout/main')

@section('title')
    <title>Acceuil</title>
@endsection

@section('content')

    {{-- Banniere --}}
    <section id="hero" style="max-height: 500px" class="pt-5 pb-0">
        <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide 1 -->
            <div class="carousel-item active" style="background-image: url(Front/assets/img/banniere/images1.jpg); background-size: 100% 500px">
            <div class="carousel-container">
                <div class="container">
                    <marquee behavior="scroll" class="p-1 bg-color-warning fs-3" style="background-color: rgba(49, 42, 42, 0.073); color: white;">
                        {{ $annoncelast->titre}}
                    </marquee>
                <h2 class="animate__animated animate__fadeInDown">Bienvenue sur la plateforme du  
                    <strong>
                        <h1 class="app">
                            {{-- type write zone d'ecriture --}}
                        </h1>
                    </strong>
                </h2>

                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Plus d'informations</a>
                </div>
            </div>
            </div>
            <!-- Slide 2 -->
            <div class="carousel-item" style="background-image: url(Front/assets/img/banniere/images5.jpg); background-size: 100% 500px">
            <div class="carousel-container">
                <div class="container">
                <h2 class="animate__animated animate__fadeInDown">Bienvenue sur la plateforme du <span>MENUTI</span></h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Plus d'informations</a>
                </div>
            </div>
            </div>
            <!-- Slide 3 -->
            <div class="carousel-item" style="background-image: url(Front/assets/img/banniere/images4.jpg); background-size: 100% 500px">
            <div class="carousel-container">
                <div class="container">
                <h2 class="animate__animated animate__fadeInDown">Bienvenue sur la plateforme du <span>MENUTI</span></h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Plus d'informations</a>
                </div>
            </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
            <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
            <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
        </a>
        </div>
    </section>
    {{-- End banniere --}}

    {{--contain  Actulites --}}
    <section id="portfolio" class="portfolio">
        <div class="row px-0 mx-0">
                {{-- Actualités --}}
            <div class="col-8">
                            <!-- ======= Portfolio Section ======= -->
                <div class="container" data-aos="fade-up">

                    <div class="section-title">
                        <h2>Activités</h2>
                        <p>Activités</p>
                    </div>

                    <div class="row" data-aos="fade-up" data-aos-delay="100">
                        <div class="col-lg-12 d-flex justify-content-center">
                            <ul id="portfolio-flters">
                               
                                @foreach ($Catactivites as $Catactivite)
                                    <li data-filter=".{{$Catactivite->id}}" class="filter">{{$Catactivite->titre}}</li>
                                @endforeach
                         
                            </ul>
                        </div>
                    </div>

                   
                    <div class="row portfolio-container mt-4 col-md-12 mx-auto px-auto " data-aos="fade-up" data-aos-delay="200" style="margin-left: 100px !important">
                            @forelse ($activites as $activite)

                                <div class="col-lg-3 col-md-3 portfolio-item {{$activite->CatByActivite->id}} card-fifed mx-4 px-0 ">
                                   <a href="{{ route('activites_show', $activite->id) }}">
                                        <img src="{{ asset("uploads/$activite->image") }}" class="img-fluid" alt="" style="min-width:250px !important">
                                        <div class="portfolio-info text-center d-flex  justify-content-center">

                                            <h4>{{Str::limit($activite->titre, 40)}}</h4>

                                        </div>
                                   </a>
                                </div>
                            @empty
                                <span>Aucune activité publiée</span>
                            @endforelse
                    </div>
                   

                </div>
            </div>
            {{-- End contain Actualités --}}

            {{-- Calendrier --}}
            <div class="col-4 pt-3 mb-0" style="margin-top: 150px !important">
                <div id="why-us" class="why-us section-bg m-0">
                    <div class="container-fluid" data-aos="fade-up">
                    <div class="row">
                        <div class="col-lg-12 px-0 d-flex flex-column justify-content-center align-items-stretch">
                        <div class="content  m-0 p-0 py-3" >
                            <h3><strong>Evenements de la semaine</strong></h3>
                        </div>
                        <div class="accordion-list mx-0 px-0">
                            <ul>
                                @forelse ($annonces as $annonce)
                                    <li>
                                        <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-{{$annonce->id}}"><span>{{$annonce->date}}</span>{{$annonce->titre}} <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                        <div id="accordion-list-{{$annonce->id}}" class="collapse " data-bs-parent=".accordion-list">
                                        <p>
                                            {{$annonce->libelle}}
                                        </p>
                                        </div>
                                    </li>
                                @empty
                                    <span>Aucune annonce publiée</span>
                                @endforelse
                            {{-- <li>
                                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>25</span> Sommet de nomination des  DSI<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                                <p>
                                    Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                                </p>
                                </div>
                            </li>
                            <li>
                                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span> Dolor sit amet consectetur adipiscing elit? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                                <p>
                                    Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                                </p>
                                </div>
                            </li>
                            <li>
                                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4" class="collapsed"><span>04</span> Dolor sit amet consectetur adipiscing elit? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-4" class="collapse" data-bs-parent=".accordion-list">
                                <p>
                                    Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                                </p>
                                </div>
                            </li> --}}
                            </ul>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            {{-- End calendrier --}}
        </div>
    </section>
    {{-- End Actualites --}}

    {{-- Portail metier --}}

    <section id="services" class="services pt-0 pb-4 m-0">
        <div class="container-fluid" data-aos="fade-up">

        <div class="section-title">
            <h2>Portail métier</h2>
            <p>Portail métier</p>
        </div>

        <div class="row">
            <div class="col-lg-2 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
                <div class="icon"><i class="bx bxl-dribbble"></i></div>
                <h4><a href="">Lorem Ipsum</a></h4>
                <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
            </div>
            </div>

            <div class="col-lg-2 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
                <div class="icon"><i class="bx bx-file"></i></div>
                <h4><a href="">Sed ut perspiciatis</a></h4>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
            </div>
            </div>

            <div class="col-lg-2 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
                <div class="icon"><i class="bx bx-tachometer"></i></div>
                <h4><a href="">Magni Dolores</a></h4>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
            </div>
            </div>

            <div class="col-lg-2 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box">
                <div class="icon"><i class="bx bxl-dribbble"></i></div>
                <h4><a href="">Lorem Ipsum</a></h4>
                <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
                <div class="icon-box">
                    <div class="icon"><i class="bx bx-file"></i></div>
                    <h4><a href="">Sed ut perspiciatis</a></h4>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
                <div class="icon-box">
                <div class="icon"><i class="bx bx-tachometer"></i></div>
                <h4><a href="">Magni Dolores</a></h4>
                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                </div>
            </div>

        </div>

        </div>
    </section>

    {{-- End Portail metier --}}



    {{-- Footer --}}
    <footer id="footer">
            <div class="footer-top">
            <div class="container">
                <div class="row">
                <div class="col-lg-12">
                    <div class="footer-info">
                    <h3>MENUTI</h3>
                        <p>
                            Ave Marchand, Abidjan <br>
                        Postel 2001, Cote d'ivoire<br><br>
                            <strong>Phone:</strong> +1 5589 55488 55<br>
                            <strong>Email:</strong> info@example.com<br>
                        </p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
    </footer>
    {{-- End Footer --}}
@endsection
