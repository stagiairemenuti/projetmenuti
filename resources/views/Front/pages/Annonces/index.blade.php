@extends('Front/layout/main')

@section('title')
    <title>Annonces</title>
@endsection

@section('content')
    {{-- Anonces --}}
    <section id="faq" class="faq pt-7" style="border-left: solid 1px #ed502e; border-right: solid 1px #ed502e; margin: 50px">
        <div class="container" data-aos="fade-up">

          <div class="section-title">
            <h2>Annonces</h2>
            <p>Annonces</p>
          </div>

          <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="col-lg-2">
              <h4 style="color: #ed502e">Dates</h4>
            </div>

            <div class="col-lg-8">
                <h4 style="color: #ed502e">Libéllés</h4>
            </div>

            <div class="col-lg-2">
                <h4 style="color: #ed502e">Publier le</h4>
            </div>

          </div>

          @forelse ($annonces as $annonce)
            <div class="row faq-item d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
              <div class="col-lg-2">
                <i class="bx bx-help-circle"></i>
                <h4 style="color: #ed502e">{{$annonce->date}}</h4>
              </div>
              <div class="col-lg-8">
                <p>
                  {{$annonce->libelle}}
                </p>
              </div>
              <div class="col-lg-2">
                <p>
                  {{$annonce->created_at}}
                </p>
              </div>
            </div><!-- End F.A.Q Item-->
          @empty
              <span>Aucune Annonce Publiée</span>
          @endforelse

         

        </div>
    </section>
{{-- End Annonces --}}
@endsection
