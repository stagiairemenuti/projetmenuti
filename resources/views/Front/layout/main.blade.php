<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    @yield('title')
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link href="{{asset ('Front/assets/img/amoirie.png') }}" rel="icon">
    <link href="{{asset('Front/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="{{ ('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i') }}" rel="stylesheet">
    <!-- Vendor CSS Files -->
    <link href="{{asset ("Front/assets/vendor/animate.css/animate.min.css") }}" rel="stylesheet">
    <link href="{{asset ('Front/assets/vendor/aos/aos.css" rel="stylesheet') }}">
    <link href="{{asset ('Front/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{asset ('Front/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{asset ('Front/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{asset ('Front/assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{asset ('Front/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{asset ('Front/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="{{asset ('Front/assets/css/style.css') }}" rel="stylesheet">
    
    <script src="https://unpkg.com/typewriter-effect@latest/dist/core.js"></script>


   

    <link rel="stylesheet" href="{{asset ("Front/assets/tab.css") }}">
    <link rel="stylesheet" href="{{asset ("Front/assets/calendrier.css") }}">
    {{-- calendier --}}
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
{{-- Fin calendrier --}}
    <!-- =======================================================
    * Template Name: Multi - v4.7.0
    * Template URL: https://bootstrapmade.com/multi-responsive-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body class="pb-0">
  <!-- ======= Header ======= -->
    <footer id="header" class="fixed-top">
      <div class="container d-flex align-items-center justify-content-between">
          <a href="{{route('home')}}"><img src="{{asset ('Front/assets/logo.png') }}" style="width: 150px"></a>

        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        <nav id="navbar" class="navbar">
          <ul>
            <li><a class="nav-link scrollto" href="/">Acceuil</a></li>
            <li><a class="nav-link scrollto" href="{{route('activites_front')}}">Activités</a></li>
            <li><a class="nav-link scrollto" href="{{route('annonce_front')}}">Annonces</a></li>
            <li><a class="nav-link scrollto" href="{{route('documents_front')}}">Documents</a></li>
            <li><a class="nav-link scrollto" href="{{route('trombinoscope_front')}}">Trombinoscope</a></li>
            <li><a class="nav-link scrollto" href="{{route('calendrier_front')}}">Calendrier</a></li>
            <li><a class="nav-link scrollto badge badge-success" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Deconnexion</a>
              <form action="{{ route('logout') }}" id="logout-form" method="post">@csrf</form></li>
          </ul>
          <i class="bi bi-list mobile-nav-toggle"></i>
          
        </nav><!-- .navbar -->
      </div>
    </footer><!-- End Header -->

    <div>
            @yield('content')
    </div>






    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <!-- Vendor JS Files -->
    <script src="{{asset ('Front/assets/vendor/purecounter/purecounter.js') }}"></script>
    <script src="{{asset ('Front/assets/vendor/aos/aos.js') }}"></script>
    <script src="{{asset ('Front/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{asset ('Front/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{asset ('Front/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{asset ('Front/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{asset ('Front/assets/vendor/php-email-form/validate.js') }}"></script>
    <!-- Template Main JS File -->
    <script src="{{asset ('Front/assets/js/main.js') }}"></script>
    <script src="{{asset ('Front/assets/calendrier.js') }}"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="{{asset('Front\js\script.js') }}"></script>

    {{-- Bootstrap --}}
</body>
</html>
