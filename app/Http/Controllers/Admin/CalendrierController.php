<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Calendrier;
use Illuminate\Http\Request;

class CalendrierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calendriers = Calendrier::whereEtat(true)->get();

        return view('Admin.Dashbord.pages.Calendrier.index',compact('calendriers'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Dashbord.pages.Calendrier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $calendriers=Calendrier::create(
            [   
                'titre'=> $request->titre,
                'date'=> $request->date,
                'etat' => true,
            ]
        )->save();

        if( $calendriers ){
            return redirect()->back()->with('success','Vous venez d\'enregistrer un calendriers');

        }else{
            return redirect()->back()->with('fail','Something went wrong, failed to register');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editcalendrier = Calendrier::find($id);

        return view('Admin.Dashbord.pages.Calendrier.edit',compact('editcalendrier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $calendriers = Calendrier::whereId($id)->update(
            [
                'date'=> $request->date,
                'titre'=> $request->titre,
                'type_id'=> $request->type_id,
                'etat' => true,
            ]
        );

        return redirect()->back()->with('success','Le Calendrier a été modifier avec success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $descalendrier = Calendrier::find($id);
    
        $descalendrier -> update(
            [
                'etat'=> false,
        ]);

        return redirect()->back()->with('success','Article supprimé avec success');
 
    }
}
