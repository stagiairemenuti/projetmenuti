<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\Activite;
use App\Models\Admin\CatActivite;
use App\Http\Controllers\Controller;

class ActiviteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $activites = Activite::whereEtat(true)->get();

        $catactivites=CatActivite::whereEtat(true)->get();

        // dd($activites);

        return view('Admin/Dashbord/pages/Activite/index',compact('activites','catactivites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $catactivites=CatActivite::whereEtat(true)->get();

        // dd($catactivites);

        return view('Admin/Dashbord/pages/Activite/create',compact('catactivites'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file=$request->image;
        if ($file) {
            $fileName = time() . '-' . $file->getClientOriginalName();
            $file->move(public_path('uploads'), $fileName);
        } 

       
        
        $activite=Activite::create(
            [
                'titre'=> $request->titre,
                'libelle'=> $request->libelle,
                'category_Id'=> $request->category_Id,
                'image'=> $fileName ?? "",
                'etat' => true,
            ]
        )->save();

        // dd($activite);

        if( $activite ){
            return redirect()->back()->with('success','Vous venez d\'enregistrer une activite');
            // return redirect()->intended(route('show.activite'));
        }else{
            return redirect()->back()->with('fail','Something went wrong, failed to register');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $showactivite = Activite::find($id);
        
        return view('Front/pages/Activites/show',compact('showactivite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editactivite = Activite::find($id);

        // dd($editactivite);

        return view('Admin/Dashbord/pages/Activite/edit',compact('editactivite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file=$request->image;
        if ($file) {
            $fileName = time() . '-' . $file->getClientOriginalName();
            $file->move(public_path('uploads'), $fileName);
        } 

       
        
        $editactivite = Activite::whereId($id)->update(
            [
                'titre'=> $request->titre,
                'libelle'=> $request->libelle,
                'image'=> $fileName ?? "",
                'category_Id'=> $request->category_Id,
            ]
        );

        return redirect()->back()->with('success','L\'Article a été modifier avec success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $editactivite = Activite::find($id);
        
        $editactivite -> update(
            [
                'etat'=> false,
            ]);

            return redirect()->back()->with('success','Article supprimé avec success');
    }
}
