<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Annonce;
use Illuminate\Http\Request;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $annonces=Annonce::whereEtat(true)->get();

        // dd($annonces);

        return view('Admin.Dashbord.pages.Annonce.index',compact('annonces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Dashbord.pages.Annonce.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     


    public function store(Request $request)
    {
        $annonces=Annonce::create(
            [
                'date'=> $request->date,
                'titre'=> $request->titre,
                'libelle'=> $request->libelle,
                'etat' => true,
            ]
        )->save();

        if( $annonces ){
            return redirect()->back()->with('success','Vous venez d\'enregistrer un article');

        }else{
            return redirect()->back()->with('fail','Something went wrong, failed to register');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editannonce=Annonce::find($id);

        return view('Admin.Dashbord.pages.Annonce.edit',compact('editannonce'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $thisannonce = Annonce::whereId($id)->update(
            [
                'date'=> $request->date,
                'titre'=> $request->titre,
                'libelle'=> $request->libelle,
                'etat' => true,
            ]
        );

        return redirect()->back()->with('success','L\'Annonce a été modifier avec success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $editannonces = Annonce::find($id);

        $editannonces -> update(
            [
                'etat'=> false,
            ]);

            return redirect()->back()->with('success','Article supprimé avec success');
    }
}
