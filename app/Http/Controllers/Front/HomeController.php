<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Models\Admin\Activite;
use App\Models\Admin\CatActivite;
use App\Http\Controllers\Controller;
use App\Models\Admin\Annonce;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activites = Activite::whereEtat(true)->get();

        $annonces = Annonce::whereEtat(true)->limit(4)->get();
        
        $annoncelast = Annonce::whereEtat(true)->first();

        $Catactivites = CatActivite::whereEtat(true)->get();
        // dd($annoncelast);
        return view('Front/pages/index',compact('activites','Catactivites','annonces','annoncelast'));
    }

    public function indexActivites()
    {
        
        return view('Front/pages/Activites/index');
    }
    
}
