<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calendrier extends Model
{
    use HasFactory;

    protected $table = 'calendriers';

    protected $fillable = [

        'titre',
        'date',
        'etat',
        
    ];
}
