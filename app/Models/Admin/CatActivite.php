<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatActivite extends Model
{
    use HasFactory;

    // protected $table = 'cat_activites';

    protected $fillable = [

        'titre',
        'etat',
    ];


  
}
