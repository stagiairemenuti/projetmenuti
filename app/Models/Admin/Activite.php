<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Activite extends Model
{
    use HasFactory;

    protected $fillable = [
        'titre',
        'libelle',
        'image',
        'category_Id',
        'etat'
    ];

    public function CatByActivite (){
        
        return $this->belongsTo('App\Models\Admin\CatActivite', 'category_Id', 'id');
    }
}
