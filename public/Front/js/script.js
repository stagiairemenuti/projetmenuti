$(document).ready(function() {

    const txtAnim = document.querySelector('.app');

    new Typewriter(txtAnim, {
        // loop: true,
        deleteSpeed: 40
    })
    .changeDelay(40)
    .typeString('<span style="font-size:80px; font-family:New Tegomin"> MCEN </span>')
    .pauseFor(300)
    .pauseFor('1000')
    .start()

});